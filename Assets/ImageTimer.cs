using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageTimer : MonoBehaviour
{
    public AudioSource source;
    public AudioClip clip;

    public float MaxTime;
    public bool Tick;

    private Image img;
    private float currentTime;
    
    void Start()
    {
        img = GetComponent<Image>();
        currentTime = MaxTime;
    }

    void Update()
    {
        Tick = false;
        currentTime -= Time.deltaTime;

        if (currentTime <= 0)
        {
            Tick = true;
            currentTime = MaxTime;
            source.PlayOneShot(clip);
        }

        img.fillAmount = currentTime / MaxTime;
    }
}
