using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public ImageTimer HarvestTimer;
    public ImageTimer EatingTimer;
    public Image RaidTimerImg;
    public Image PeasantTimerImg;
    public Image WarriorTimerImg;

    public Button peasantButton;
    public Button warriorButton;
    public Button pauseButton;
    public Button unpauseButton;

    public Text resourcesText;
    public Text raidersText;
    public Text lapsUntilAttacksStart;
    public Text gameOverText;
    public Text youWinText;

    public GameObject GameOverScreen;
    public GameObject YouWinScreen;
    public GameObject PauseScreen;

    public AudioSource source;
    public AudioClip RaidClip;
    public AudioClip WarriorClip;
    public AudioClip PeasantClip;

    public int peasantCount;
    public int warriorCount;
    public int wheatCount;

    public int wheatPerPeasant;
    public int wheatToWarriors;

    public int peasantCost;
    public int warriorCost;

    public float peasantCreateTime;
    public float warriorCreateTime;
    public float raidMaxTime;
    public int raidIncrease;
    public int nextRaid;
    public int lapsUntilAttacksStartCount;

    private float peasantTimer = -2;
    private float warriorTimer = -2;
    private float raidTimer;
    private int raidsSurvivedCount = -1;

    void Start()
    {
        UpdateText();
        raidTimer = raidMaxTime;
    }

    void Update()
    {
        raidTimer -= Time.deltaTime;
        RaidTimerImg.fillAmount = raidTimer / raidMaxTime;

        if (raidTimer <= 0)
        {
            raidTimer = raidMaxTime;
            if (lapsUntilAttacksStartCount <= 0)
            {
                warriorCount -= nextRaid;
                nextRaid += raidIncrease;
                raidsSurvivedCount++;
                source.PlayOneShot(RaidClip);
            }
            else
            {
                lapsUntilAttacksStartCount--;
            }
        }

        if (HarvestTimer.Tick)
        {
            wheatCount += peasantCount * wheatPerPeasant;
        }    
        
        if (EatingTimer.Tick)
        {
            wheatCount -= warriorCount * wheatToWarriors;
        }

        if (peasantTimer > 0)
        {
            peasantTimer -= Time.deltaTime;
            PeasantTimerImg.fillAmount = peasantTimer / peasantCreateTime;
        }
        else if (peasantTimer > -1)
        {
            PeasantTimerImg.fillAmount = 1;
            peasantButton.interactable = true;
            peasantCount += 1;
            peasantTimer = -2;
            source.PlayOneShot(PeasantClip);

        }

        if (warriorTimer > 0)
        {
            warriorTimer -= Time.deltaTime;
            WarriorTimerImg.fillAmount = warriorTimer / warriorCreateTime;
        }
        else if (warriorTimer > -1)
        {
            WarriorTimerImg.fillAmount = 1;
            warriorButton.interactable = true;
            warriorCount += 1;
            warriorTimer = -2;
            source.PlayOneShot(WarriorClip);
        }

        if (warriorCount < 0)
        {
            Time.timeScale = 0;
            GameOverScreen.SetActive(true);
        }

        if (wheatCount >= 500)
        {
            Time.timeScale = 0;
            YouWinScreen.SetActive(true);
        }

        UpdateText();
    }

    public void CreatePeasant()
    {
        if (wheatCount > peasantCost)
        {
            wheatCount -= peasantCost;
            peasantTimer = peasantCreateTime;
            peasantButton.interactable = false;
        }
    }

    public void CreateWarrior()
    {
        if (wheatCount > warriorCost)
        {
            wheatCount -= warriorCost;
            warriorTimer = warriorCreateTime;
            warriorButton.interactable = false;
        }
    }

    public void UpdateText()
    {
        resourcesText.text = peasantCount + "\n" + warriorCount + "\n\n" + wheatCount;
        raidersText.text = nextRaid.ToString();
        lapsUntilAttacksStart.text = lapsUntilAttacksStartCount.ToString();
        gameOverText.text = raidsSurvivedCount + "\n" + wheatCount + "\n" + peasantCount;
        youWinText.text = raidsSurvivedCount + "\n" + wheatCount + "\n" + peasantCount;
    }

    public void Pause()
    {
            Time.timeScale = 0;
            PauseScreen.SetActive(true);
    }

    public void Unpause()
    {
        Time.timeScale = 1;
        PauseScreen.SetActive(false);
    }

    public void Mute()
    {
        if (AudioListener.pause == false)
        {
            AudioListener.pause = true;
        }
        else
        {
            AudioListener.pause = false;
        }
    }    
}
