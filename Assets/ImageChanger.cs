using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageChanger : MonoBehaviour
{
    public Sprite newSprite;
    private Image img;
    private AudioSource audio;
    // Start is called before the first frame update
    void Start()
    {
        img = GetComponent<Image>();
        audio = GetComponent<AudioSource>();
        audio.Play();
    }

    public void ChangeSprite()
    {
        img.sprite = newSprite;
        img.SetNativeSize();
    }
    public void ChangeColor()
    {
        img.color = Color.magenta;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
